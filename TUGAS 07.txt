1. Membuat Database

CREATE DATABASE myshop;

2. Membuat Tabel
-tabel users:
CREATE TABLE users(id int(4) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null); 

-tabel items:
CREATE TABLE itmes( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null ); 

-tabel categories:
CREATE TABLE itmes( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price integer not null, stock integer not null, category_id integer not null, FOREIGN KEY(category_id) REFERENCES categories(id) ); 

3. Memasukkan Data

- tabel users 
INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"); 
INSERT INTO users(name, email, password) VALUES("Jane Doe", "jane@doe.com", "jenita123"); 

- tabel categories
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded"); 

- tabel items
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1); 

4. Mengambil Data dari Databases

a. Mengambil data users kecuali field password
SELECT id, name, email from users; 

b. Mengambil data items 
point 1 : SELECT * from items where price > 1000000; 
point 2 : SELECT * from items where name like "%uniklo%"; 

c. Menampilkan data items join dengan kategori

SELECT items.name, items.description, items.price, items.stock, categories.id as category_id from items inner join categories on items.category_id=categories.id; 

5. Mengubah Data dari Database

UPDATE items set price = 2500000 where name = "Sumsang b50";  

